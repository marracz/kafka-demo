# Kafka & Hazelcast FlakeId Generator

Simple Spring-Boot application integrated with Apache Kafka, Elasticsearch and Hazelcast.
It allows trigger log events on endpoint `/test`, consumes them and persists in Elasticsearch.

Application can be run with specific implementation of FlakeId generation algorithm:

* custom
* Hazelcast embedded algorithm for id generation 

Algorithm can be specified by `generator` property.
