package com.example.kafkademo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.IntStream;

@RestController
public class WebController {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final String topic;

    @Autowired
    public WebController(KafkaTemplate<String, String> kafkaTemplate,
                         @Value("${kafka.topic}") String topic) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void produceMessages() {
        IntStream.range(0, 5000)
                .forEach(it -> kafkaTemplate.send(topic, String.valueOf(it%2),"message" + it));
    }
}