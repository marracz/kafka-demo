package com.example.kafkademo.service;

import com.hazelcast.cluster.Member;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@ConditionalOnProperty(value = "generator", havingValue = "custom")
public class CustomFlakeLogIdGenerator implements LogIdGenerator {

    private long counter = 1;
    private long lastTimestamp = 0;

    private final HazelcastInstance hzInstance;

    @Autowired
    public CustomFlakeLogIdGenerator(HazelcastInstance hzInstance) {
        this.hzInstance = hzInstance;
    }

    @Override
    public long generateId() {
        long time = System.currentTimeMillis();
        long id = time * 1000 + counter++;

        if (time > lastTimestamp) {
            counter = 1;
            lastTimestamp = time;
        }
        return id * 10 + getNumberOfLocalMember();
    }

    private int getNumberOfLocalMember() {
        Set<Member> members = hzInstance.getCluster().getMembers();
        int x = 0;
        for (Member m : members) {
            if (m.localMember()) {
                return x;
            } else {
                x++;
            }
        }
        return x;
    }

}
