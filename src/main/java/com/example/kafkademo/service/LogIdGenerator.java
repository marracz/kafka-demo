package com.example.kafkademo.service;

public interface LogIdGenerator {
    long generateId();
}
