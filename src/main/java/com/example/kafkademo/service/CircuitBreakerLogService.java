package com.example.kafkademo.service;

import com.example.kafkademo.model.Log;
import com.example.kafkademo.repository.LogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@EnableScheduling
@Service
public class CircuitBreakerLogService {

    private final LogRepository logRepository;
    private final AtomicLong failsCounter;
    private static final long FAILS_THRESHOLD = 5;

    public CircuitBreakerLogService(LogRepository logRepository) {
        this.logRepository = logRepository;
        this.failsCounter = new AtomicLong(0);
    }

    public void registerLog(long id, String message, int partition) {
        if (failsCounter.get() > FAILS_THRESHOLD) {
            log.info("Circuit breaker is open");
            throw new RuntimeException("Circuit breaker is open");
        } else {
            try {
                log.info("Circuit breaker is closed");
                logRepository.save(new Log(id, message, partition));
            } catch (Exception ex) {
                failsCounter.incrementAndGet();
                throw ex;
            }
        }
    }

    @Scheduled(fixedDelay = 60000)
    public void resetFailsCounter() {
        if (failsCounter.get() > FAILS_THRESHOLD) {
            failsCounter.set(0);
        }
    }
}
