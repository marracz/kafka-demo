package com.example.kafkademo.service;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(value = "generator", havingValue = "hazelcast")
public class HazelcastFlakeLogIdGenerator implements LogIdGenerator {

    private final HazelcastInstance hzInstance;
    private static final String LOGS_ID_GENERATOR_NAME = "logs";

    @Autowired
    public HazelcastFlakeLogIdGenerator(HazelcastInstance hzInstance) {
        this.hzInstance = hzInstance;
    }

    @Override
    public long generateId() {
        return hzInstance
                .getFlakeIdGenerator(LOGS_ID_GENERATOR_NAME)
                .newId();
    }
}
