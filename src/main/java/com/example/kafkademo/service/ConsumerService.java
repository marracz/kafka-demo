package com.example.kafkademo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConsumerService {

    private final LogIdGenerator logIdGenerator;
    private final CircuitBreakerLogService circuitBreakerLogService;

    @Autowired
    public ConsumerService(LogIdGenerator logIdGenerator,
                           CircuitBreakerLogService circuitBreakerLogService) {
        this.logIdGenerator = logIdGenerator;
        this.circuitBreakerLogService = circuitBreakerLogService;
    }

    @KafkaListener(topics = "${kafka.topic}")
    public void listenWithHeaders(
            @Payload String message,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {

        long id = logIdGenerator.generateId();
        log.info("Received message: " + message + " from partition: " + partition + " with id " + id);
        circuitBreakerLogService.registerLog(id, message, partition);
    }

}
