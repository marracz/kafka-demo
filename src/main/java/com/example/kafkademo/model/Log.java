package com.example.kafkademo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@AllArgsConstructor
@Data
@Document(indexName = "logs")
public class Log {

    @Id
    private long id;
    private String message;
    private int partition;
}
