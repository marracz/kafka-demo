package com.example.kafkademo;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
public class DynatraceTest {

    @Test
    public void test() {
        System.out.println(solution(268));
        System.out.println(solution(-268));
    }

    public int solution(int N) {
        // write your code in Java SE 8
        Set<Integer> allNumbers = new HashSet<>();
        String Nstr = String.valueOf(N);
        int start = 0;
        if (N < 0) {
            start = 1;
        }
        for (int i=start; i<=Nstr.length(); i++) {
            final String numberStr = Nstr.substring(0, i) + "5" + Nstr.substring(i);
            final Integer number = Integer.valueOf(numberStr);
            allNumbers.add(number);
        }
        return allNumbers.stream().max(Integer::compareTo).orElse(0);
    }

    @Test
    public void test2() {
        String[] x = new String[] {"co", "dil", "ity"};
        final int res1 = solution2(x);
        System.out.println(">" + res1);
        //assertEquals(solution2(x), 5);
        String[] y = new String[] {"abc", "yyy", "def", "csv"};
        final int res2 = solution2(y);
        System.out.println(">" + res2);
        //assertEquals(solution2(y), 6);
    }

    public int solution2(String[] A) {
        // write your code in Java SE 8
        String concatenatedResult = "";
        for (int i=0; i<A.length-1; i++) {
//            for (int j=0; j<A.length; j++) {
//                if (i==j) continue;
//                if (hasDuplicates(A[i], A[j])) {
//                    System.out.println(A[i] + " has dups " + A[i+1]);
//                } else {
//                    concatenatedResult = concatenatedResult.concat(A[i]);
//                }
//            }
            if (hasDuplicates(A[i], concatenatedResult)) {
                System.out.println(A[i] + " has dups " + A[i+1]);
            } else {
                System.out.println(A[i] + " has no dups " + A[i+1]);
                concatenatedResult = concatenatedResult.concat(A[i]);
            }
        }
        System.out.println(concatenatedResult);
        return concatenatedResult.length();
    }

    private boolean hasDuplicates(String s1, String s2) {
        Map<Character, Integer> occurrences = new HashMap<>();
        addOccurrences(s1, occurrences);
        addOccurrences(s2, occurrences);
        return occurrences.values().stream().anyMatch(x -> x > 1);
    }

    private void addOccurrences(String s1, Map<Character, Integer> occurrences) {
        for (int i = 0; i < s1.length(); i++) {
            occurrences.put(s1.charAt(i), occurrences.getOrDefault(s1.charAt(i), 0)+1);
        }
    }
}
